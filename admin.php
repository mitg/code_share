<?php
/* @var $this QuotepdfsController */
/* @var $model ConfigQuotepdf */

	$this->breadcrumbs=array(
		'Config Quotepdfs'=>array('index'),
		'Manage',
	);

	$this->menu=array(
		array('label'=>'List ConfigQuotepdf', 'url'=>array('index')),
		array('label'=>'Create ConfigQuotepdf', 'url'=>array('create')),
	);

	Yii::app()->clientScript->registerScript('search', "
		$('.search-button').click(function(){
			$('.search-form').toggle();
			return false;
		});
		$('.search-form form').submit(function(){
			$('#config-quotepdf-grid').yiiGridView('update', {
				data: $(this).serialize()
			});
			return false;
		});
	");

	$str_js = "
		var fixHelper = function(e, ui) {
			ui.children().each(function() {
				$(this).width($(this).width());
			});
			return ui;
		};
				
		$('#config-quotepdf-grid table.items tbody').sortable({
			forcePlaceholderSize: true,
			forceHelperSize: true,
			items: 'tr',
			update : function () {
				serial = $('#config-quotepdf-grid table.items tbody').sortable('serialize', {key: 'items[]',
				 attribute: 'class'});
				$.ajax({
					'url': '" . $this->createUrl('//configQuotepdfs/sort') . "',
					'type': 'post',
					'data': serial,
					'success': function(data){
					},
					'error': function(request, status, error){
						alert('We are unable to set the sort order at this time.  Please try again in a few
						 minutes.');
					}
				});
			},
			helper: fixHelper
		}).disableSelection();
	";

Yii::app()->clientScript->registerScript('sortable-configQuotepdf', $str_js);

?>

<h1><?php echo Yii::t("manageQuotePdfContractor", "Manage ConfigQuotepdf"); ?></h1>

<?php 
	echo $this->renderPartial('_menu', array(
	    'list' => array(
	        CHtml::link(Yii::t('createConfigQuotepdf', 'Create Quote PDF Pages'), array('create')),
	    ),
	)); 
	if((!Yii::app()->user->checkAccess('client')) || (!Yii::app()->user->isGuest)) { 
			echo CHtml::link('Advanced Search','#',array('class'=>'search-button'));
	 } 
?>

<div class="search-form" style="display:none">
	<?php $this->renderPartial('_search',array('model'=>$model)); ?>
</div><!-- search-form -->

<div class="datawraper">
    <div class="columnheading">
		<?php $this->widget('zii.widgets.grid.CGridView', array(
				'id'=>'config-quotepdf-grid',
				'dataProvider'=>$model->search(),
				'rowCssClassExpression'=>'"items[]_{$data->id}"',
				'cssFile' => Yii::app()->theme->baseUrl . '/css/gridStyle.css',
				'filter'=>$model,
				'afterAjaxUpdate' => "js:function(){".$str_js."}",
				'columns'=>array(
					'quote_type',
					'page_type',
					'file_type',
					'position',
					'file_name',

					array(
		                'header' => "Delete",
		                'class' => "CButtonColumn",
		                'template' => "{view}{active}{inactive}{update}{delete}",
		                'htmlOptions'=>array('width'=>'100px'),
		                'buttons' => array (
		                	'view' => array
	                        (
	                            'imageUrl' => Yii::app()->theme->baseUrl . '/images/icons/view_icon.png',
	                            'visible' => "((Yii::app()->user->checkAccess('Admin')) ? true : false)",
	                            'options'=>array('class'=>'grid-view-btn'),
	                        ),
		                	'update' => array
	                        (
	                            'imageUrl' => Yii::app()->theme->baseUrl . '/images/icons/setting_icon.png',
	                            'visible' => "((Yii::app()->user->checkAccess('Admin')) ? true : false)",
	                        ),
	                    	'delete' => array
	                        (
		                        'imageUrl' => Yii::app()->theme->baseUrl . '/images/icons/delete_icon.png',
		                        'visible' => "((Yii::app()->user->checkAccess('Admin')) ? true : false)",
	                    	),
		                    'active' => array (
		                    	'label' => 'Click to inactivate page',
		                        'visible' => '(Yii::app()->user->checkAccess("Admin") && $data->status=="active") ? true : false',
		                        'imageUrl' => Yii::app()->theme->baseUrl . '/images/active.png',
		                        'click' => "function(){
		                                $.fn.yiiGridView.update('config-quotepdf-grid', {
		                                    type:'POST',
		                                    url:$(this).attr('href'),
		                                    success:function(data) {
		                                        $.fn.yiiGridView.update('config-quotepdf-grid');
		                                    }
		                                })
		                                return false;
		                            }",
		                        'url' => 'Yii::app()->createUrl( "configQuotepdfs/changestatus", array("id"=>$data->id, "status" => "inactive", "table" => "config_quotepdf"))',
		                        'options'=>array('class'=>'grid-active-btn'),
		                    ),
		                    'inactive' => array (
		                        'label' => 'Click to activate page',
		                        'visible' => '(Yii::app()->user->checkAccess("Admin") && $data->status == "inactive") ? true : false',
		                        'imageUrl' => Yii::app()->theme->baseUrl . '/images/inactive.png',
		                        'click' => "function() {
		                                $.fn.yiiGridView.update('config-quotepdf-grid', {
		                                    type: 'POST',
		                                    url: $(this).attr('href'),
		                                    success: function(data) {
		                                        $.fn.yiiGridView.update('config-quotepdf-grid');
		                                    }
		                                })
		                                return false;
		                            }",
		                        'url' => 'Yii::app()->createUrl("configQuotepdfs/changeStatus", array("id" => $data->id, "status" => "active", "table" => "config_quotepdf"))',
		                        'options'=>array('class' => 'grid-inactive-btn'),
		                    ),
		            	),	
					),
				),
			)
		); ?>
 	</div>
</div>

<?php Yii::app()->clientScript->registerScriptFile('http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/jquery-ui.min.js'); ?>