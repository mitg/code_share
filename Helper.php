<?php

namespace App\Helpers;

use Mail;
use DB;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Response;

class Helper
{
    /**
     * @auth mitg
     * Generate a unique random string of characters
     * uses str_random() helper for generating the random string
     *
     * @param     $table - name of the table
     * @param     $col - name of the column that needs to be tested
     * @param int $chars - length of the random string
     *
     * @return string
     */
    public static function unique_random($table, $col, $chars = 10)
    {

        $unique = false;

        // Store tested results in array to not test them again
        $tested = [];

        do {

            // Generate random string of characters
            $random = str_random($chars);

            // Check if it's already testing
            // If so, don't query the database again
            if (in_array($random, $tested)) {
                continue;
            }

            // Check if it is unique in the database
            $count = DB::table($table)->where($col, '=', $random)->count();

            // Store the random character in the tested array
            // To keep track which ones are already tested
            $tested[] = $random;

            // String appears to be unique
            if ($count == 0) {
                // Set unique to true to break the loop
                $unique = true;
            }

            // If unique is still false at this point
            // it will just repeat all the steps until
            // it has generated a random string of characters
        } while (!$unique);

        return $random;
    }

    /**
     * @auth mitg
     * Generate a unique random string of characters
     * uses str_random() helper for generating the random string
     *
     * @param     $table - name of the table
     * @param     $col - name of the column that needs to be tested
     * @param int $chars - length of the random string
     *
     * @return string
     */
    public static function unique_deal_id($table, $col, $next_auto_increment)
    {
        $unique = false;

        // Store tested results in array to not test them again
        $tested = [];

        do {

            // Generate random string of characters
            $random = "BMD".str_pad($next_auto_increment, 9, '0', STR_PAD_LEFT);

            // If so, don't query the database again
            if (in_array($random, $tested)) {
                continue;
            }

            // Check if it is unique in the database
            $count = DB::table($table)->where($col, '=', $random)->count();

            // Store the random character in the tested array
            // To keep track which ones are already tested
            $tested[] = $random;

            // String appears to be unique
            if ($count == 0) {
                // Set unique to true to break the loop
                $unique = true;
            }

            // If unique is still false at this point
            // it will just repeat all the steps until
            // it has generated a random string of characters
        } while (!$unique);

        return $random;
    }

       
    /**
     * @auth mitg
     * @desc create custom pagination for merged objects/array data
     * @return pagination object
     */
    public static function  makeCustomPagination($items, $perPage = 10, $page = 1)
    {
        $itemsForCurrentPage = $items->forPage($page,$perPage);

        return  new \Illuminate\Pagination\LengthAwarePaginator(
            $itemsForCurrentPage, count($items), $perPage,
            \Illuminate\Pagination\Paginator::resolveCurrentPage(),
            ['path' => \Illuminate\Pagination\Paginator::resolveCurrentPath()]
        );
    }
}