@extends('layouts.user')
@section('user-content')
<div class="col-md-12">
    <div  class="blue-box-db">
        {{--*/$registrationPoint = \App\Point::where('activity_name','invitereference')->first()/*--}}
        For every friend that joins, you both earn {{$registrationPoint->point}} points. Plus {{$commissionPercent = Helper::getOption('refCommissionPercent').'%'}} of all the points they earn for life, as soon as they earn {{Helper::getOption('minPointsReqReferralPoints')}} points. <a class="info-circle" data-toggle="tooltip" data-placement="top" title="Please Note: Account must be verified"><i class="fa fa-info" aria-hidden="true"></i></a><br />
        Share your referral link : <span>@if($user->confirmed){{ URL::to('invite/' . $user->username) }} @else verify your account for your referral link @endif</span><br/>
        <div class="referal-social">@include('share.referallink',['user'=>$user])</div>
    </div>
</div>

<div class="col-md-12">
    {{-- */$totalPoints = Helper::getTotalPointsOfUser($user->id)/* --}}
    {{-- */$claimedPoints = Helper::getTotalClaimedPointsOfUser($user->id)/* --}}
    {{-- */$claimedPoints = empty($claimedPoints)? 0: $claimedPoints/* --}}
    {{-- */$claimedRewards = Helper::getClaimedRewardIds($user->id)/* --}}
    {{-- */$balancePoints = $totalPoints - $claimedPoints/* --}}

        <div class="row" id='points-section'>
            <div class="col-md-3 col-xs-6">
                <div class="reward-points">
                    <div class="rp-point">
                        <img src="{{asset('/bmd/images/earned-points.png')}}" alt="">
                        <div class="rp-text">
                            <span><?php echo number_format($balancePoints, 2); ?></span>
                            <p>Earned Points</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-xs-6">
                <div class="reward-points">
                    <div class="rp-point">
                        <img src="{{asset('/bmd/images/referral-points.png')}}" alt="">
                        <div class="rp-text">
                            <span>{{number_format(Helper::getTotalReferralPointsOfUser($user->id),2)}}</span>
                            <p>Referal Points</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-xs-6">
                <div class="reward-points">
                    <div class="rp-point">
                        <img src="{{asset('/bmd/images/remaining-today.png')}}" alt="">
                        <div class="rp-text">
                            {{--*/$maxPointsADay = Helper::getMaxPointsAllowedADay('maxPointsEarnADay')/*--}}
                            <span> {{ ($maxPointsADay - (int)Helper::getTodayTotalPointsOfUser($user->id, false))}}/{{$maxPointsADay}}</span>
                            <p>Remaining Today</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-xs-6">
                <div class="reward-points">
                    <div class="rp-point">
                        <img src="{{asset('/bmd/images/claimed-points.png')}}" alt="">
                        <div class="rp-text">
                            <span>{{$claimedPoints}}</span>
                            <p>Claimed Points</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
<div class="col-md-12">
    <div  class="red-box-db"><a id="earn-free-points" style="cursor: pointer;">Get More Points</a></div>
    @if(!empty(Auth::user()) && Auth::user()->id == $user->id)
        <div class="row" id="free-offer-wrapper">
            <ul class="nav nav-tabs nav-get-more-points">
            <li class="active"><a href="#adgem" data-toggle="tab">Apps & Surveys</a></li>
            <li><a href="#adscend" data-toggle="tab">Watch Videos</a></li>
            </ul>

            <div class="col-md-12">
                <div id="myTabContent" class="tab-content">
                    <div class="tab-pane active in" id="adgem">
                        <iframe id="offwall" class="adgem" style="border:0px;width:100%;min-height:800px;"  src="https://api.adgem.com/v1/wall?appid=61&userid={{Helper::generateADGEMSUid($user->id)}}" frameborder="0" allowfullscreen>Your browser doesn't support iframes</iframe>
                    </div>
                    <div class="tab-pane fade" id="adscend">
                        <iframe id="offwall" style="border:0px;width:100%;min-height:800px;" src="https://asmwall.com/adwall/publisher/113610/profile/12848?subid1={{Helper::generateAdscendSuid($user->id)}}" frameborder="0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
    @endif
</div>

<div class="col-md-12">
    @if(!empty(Auth::user()) && Auth::user()->id == $user->id)
    <div class="dash-reward-wrapper">
        @if($rewards->count() >  0)
            
            {{-- */$pointsZone = 0/* --}}
            {{--*/$count = 0/*--}}
            @foreach($rewards as $key => $reward)
                
                @if($totalPoints >= $reward->point && in_array($reward->id, $claimedRewards))
                    {{-- */$percentage = 100/* --}}
                @else
                    {{-- */ $percentage = (($balancePoints/$reward->point)*100) /* --}}
                    {{-- */ $percentage = ($percentage > 100)? 100 : $percentage /* --}}
                @endif

                @if($reward->type == 'mystery')
                    <h4 class="page-heading3">Rewards for {{$reward->point}} points:</h4>
                    <div class="row mystery-box">
                        <div class="col-md-12">
                            <div class="claim-block">
                                <img class="img-responsive" src="{{env('APP_URL') . env('UPLOAD_PATH') . 'rewards/' . $reward->image}}" alt="">
                                <div class="cb-containt">
                                    <div class="cb-text">
                                        <span class="big-font">{{$reward->name}}</span>
                                    </div>
                                    <div class="up-progress-bar">
                                        <div class="progress">
                                            <div data-percentage="0%" style="width: {{$percentage}}%;" class="progress-bar progress-bar-info mystery-box-progress-bar" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                        <span class="pull-left">0</span>
                                        <span class="pull-right">{{$reward->point}}</span>
                                    </div>

                                    @if(in_array($reward->id, $claimedRewards))
                                        <a href="javascript:void(0);" class="golden-btns"><span>Claimed</span></a>
                                    @elseif ($reward->point <= $balancePoints)
                                        <a href="javascript:void(0);" class="golden-btns claim" data-id="{{encrypt($reward->id)}}"><span>Claim</span></a>
                                    @else
                                        <a href="javascript:void(0);" class="gray-btns"><span>Claim</span></a>
                                    @endif
                                </div>
                                @if($createdDate = array_search($reward->id, $claimedRewards))
                                    <div class="count-down" >
                                        <div class="count-down-text-wrp">
                                        <div class="cdtw-inner">
                                            <div class="timer" data-countdown="{{date('Y/m/d', $createdDate)}}"></div>
                                            <span>Until you can claim this prize again</span>
                                        </div>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    {{--*/continue/*--}}
                @endif

                @if($pointsZone != $reward->points_zone)
                    {{-- */$pointsZone = $reward->points_zone/* --}}
                    @if($count != 0) 
                        </div>
                        <hr class="hr-blank3">     
                    @endif
                    <h4 class="page-heading3">Rewards for {{$reward->points_zone}} points:</h4>
                    <hr class="hr-blank3">
                    <div class="row row-seperator rwd-prz">
                    {{--*/$count++/*--}}
                @endif

                <div class="col-md-3 col-sm-4 rwd-prz-wrp">
                    <div class="claim-block">
                        <img class="img-responsive" src="{{env('APP_URL') . env('UPLOAD_PATH') . 'rewards/' . $reward->image}}" alt="">
                        <div class="cb-containt">
                            <div class="cb-text">
                                <span class="small-font">{{$reward->name}}</span>
                            </div>
                            <div class="up-progress-bar">
                                <div class="progress">
                                    <div data-percentage="0%" style="width: {{$percentage}}%;" class="progress-bar progress-bar-info" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <span class="pull-left">0</span>
                                <strong>
                                    {{-- */$pointToDisplay = ($balancePoints > $reward->point) ? $reward->point : $balancePoints/* --}}
                                    {{(int)$pointToDisplay}}
                                </strong>
                                <span class="pull-right">{{$reward->point}}</span>
                            </div>

                            @if(in_array($reward->id, $claimedRewards))
                                <a href="javascript:void(0);" class="blue-btns"> <span>Claimed</span></a>
                            @elseif ($reward->point <= $balancePoints)
                                <a href="javascript:void(0);" class="blue-btns claim" data-id="{{encrypt($reward->id)}}"> <span>Claim</span></a>
                            @else
                                <a href="javascript:void(0);" class="gray-btns"> <span>Claim</span></a>
                            @endif
                        </div>
                         @if($createdDate = array_search($reward->id, $claimedRewards))
                            <div class="count-down">
                                <div class="count-down-text-wrp">
                                    <div class="cdtw-inner">
                                    <div class="timer" data-countdown="{{date('Y/m/d', $createdDate)}}"></div>
                                    <span>Until you can claim this prize again</span>
                                </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            @endforeach  
            
            @if($pointsZone != 0)
                </div> <!-- default div for loop-->      
            @endif
        @else
        <p>Comming Soon...</p>
        @endif
        </div>

    @endif
</div>

</div>    
@endsection
