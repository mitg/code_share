<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Profile;
use Auth;
use Mail;
use Validator;
use App\Newsletter;
use App\UserSubscribedActivity;
use App\Activity;
use App\Referer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use App\Helpers\Helper;

class AuthController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Registration & Login Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles the registration of new users, as well as the
      | authentication of existing users. By default, this controller uses
      | a simple trait to add these behaviors. Why don't you explore it?
      |
     */

    use AuthenticatesAndRegistersUsers,
    ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';
    

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware($this->guestMiddleware(), ['except' => ['logout','verifyemail']]);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data, array $messages = array(), $isCarityUser = false)
    {
        return Validator::make($data, [
            'username' => 'required|min:3|max:70|alpha_num|unique:users',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ],$messages);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $confirmation_code = str_random(env('RAND_STR_LENGTH'));
        if(empty($data['charity_id'])) {

            $user = User::create([
                      'username' => strtolower($data['username']),
                      'email' => $data['email'],
                      'password' => bcrypt($data['password']),
                      'confirmation_code' => $confirmation_code,
                    ]);

        } else {

            $user = User::create([
                      'username' => strtolower($data['username']),
                      'email' => $data['email'],
                      'password' => bcrypt($data['password']),
                      'confirmation_code' => $confirmation_code,
                      'charity_id' => $data['charity_id'],
                    ]);
        }
        
        $profile = new Profile();

        $profile->is_completed = 1;
        $profile->avatar = Helper::randonprofilePic(); // assign default profile pic while registration
        
        $user->profile()->save($profile);
        
        //add entry in refere table if this user has came from reference of other existing user.
        $prevUrl = \Crypt::decrypt($data['ift']);
        
        if(strpos($prevUrl,'/invite/') !== false) {

          $explodedList = explode('/', $prevUrl);
          $referedByusername = array_pop($explodedList);
          $referedByusername = trim($referedByusername);
          
          if(!empty($referedByusername)) {

            $referedByUser = User::where('username',$referedByusername)->where('status','active')->where('confirmed','1')->first();
            
            if(!empty($referedByUser)) {

              Referer::create(['user_id'=> $user->id,
                'referer_id' => $referedByUser->id,
                'status' => 'inactive']);

            }  
          }
        }
        // end here

        Mail::send('auth.emails.welcome', ['user' => $user], function ($message) use ($user) {
            $message->from(env('MAIL_FROM_ADDRESS'),env('MAIL_FROM_NAME'));
            $message->to($user->email)->subject('Thanks for joining Blast My Deals');
        });
        return $user;
    }

    /*
     * Process sign in form
     * @param $request Submitted form data
     * @return json 
     */

    public function signin(Request $request) {

        $validator = Validator::make($request->all(), [
                    'login' => 'required',
                    'password' => 'required',
        ],array(
            'login.required' => 'The username/email field is required'
            ));

        if ($validator->fails()) {
            return response()->json(array(
                        'fail' => true,
                        'errors' => $validator->getMessageBag()->toArray()
            ));
        }

        if(User::loginUser($request->login, $request->password, $request->has('remember'))){
            return response()->json(array(
                        'success' => true
            ));

        } else if($message = User::checkStatus($request->login, $request->password, $request->has('remember'))){
            return response()->json(array(
                        'flash' => true,
                        'message' => $message,
            ));
            
        } else {
            return response()->json(array(
                        'flash' => true,
                        'message' => 'Please provide correct login details.'
            ));
        }
    }

    /*
     * Process sign up form
     * @param $request Submitted form data
     * @return json 
     */

    public function signup(Request $request) {
        
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            return response()->json(array(
                        'fail' => true,
                        'errors' => $validator->getMessageBag()->toArray()
            ));
        }

        if ($this->create($request->all())) {

            if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            
                $this->subscribeActivity($request->email);
                
                if($request->subscribe) {
                    Newsletter::subscribe($request->email);
                }

                return response()->json(array(
                            'success' => true
                ));
            }
        }
    }
    
    /**
     * Handle a charity user registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function charityUserRegister(Request $request)
    {
        $validator = $this->validator($request->all(),array(), true);

        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }

        if ($this->create($request->all())) {
            if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            
                // subscribe activities
                $this->subscribeActivity($request->email);
                
                if($request->subscribe) {
                    Newsletter::subscribe($request->email);
                }

                return redirect($this->redirectPath());
            }
        } else {
            $this->throwValidationException(
                $request, array('Something went wrong.')
            );
        }
    }
    
    
    /**
     * Show the application charity user registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showCharityUserRegistrationForm()
    {
        $charities = \App\charity::active()->get();
        
        return view('auth.charityuserregister',['charities' => $charities]);
    }
    
    public function postLogin(Request $request) {
        
        // get our login input
        $login = $request->input('login');
        
        // check login field
        $login_type = filter_var($login, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';
        
        // merge our login field into the request with either email or username as key
        $request->merge([ $login_type => $login]);
        
        // let's validate and set our credentials
        $this->validate($request, [
                'login' => 'required',
                'password' => 'required',
                ],array(
                'login.required' => 'The username/email field is required'
                ));
        
        if (Auth::attempt(['username' => $request->login, 'password' => $request->password], $request->has('remember'))
         || Auth::attempt(['email' => $request->login, 'password' => $request->password], $request->has('remember'))) {
        
             Auth::user()->last_activity = new \DateTime();
             Auth::user()->save();
        
            return redirect()->intended($this->redirectPath());
        
        }
        
        flash('These credentials do not match our records.', 'info');
        
        return redirect('/login')
                        ->withInput($request->only('login', 'remember'))
                        ->withErrors([
                            'login' => $this->getFailedLoginMessage(),
        ]);
    }
    
    //default active notification at registration
    public function subscribeActivity($email)
    {
        $user = User::where('email',$email)->first();
        $activities = Activity::all();
       
        $data = [];
     
        foreach ($activities as $activity) {
            $data[] = array('user_id' => $user->id,'activity_id' => $activity->id,'status' => 'Active');
        }
     
        \App\UserSubscribedActivity::insert($data);
    }
    
    public function userPreferences($email)
    {
        $user = User::where('email',$email)->first();
        $preference = \App\Preference::where('slug', 'enable-private-messaging')->where('status', 'active');
    
        if(!empty($preference)) {
           Auth::user()->preferences()->sync(array($preference->id));
        }
    }

    public function verifyemail($confirmation_code)
    {   
        if( ! $confirmation_code) {
            //throw new InvalidConfirmationCodeException;
            abort(403, 'Unauthorized action.');
        }

        $user = User::where('confirmation_code', $confirmation_code)->first();

        if ( ! $user) {

            flash('It seems that verification link is either invalid or expired.','warning');
            return redirect('/');
        }

        $user->confirmed = 1;
        $user->confirmation_code = null;
        
        // assign referal points  if applied
        $refererModel = Referer::where('user_id', $user->id)->where('status','inactive')->first();
        if(!empty($refererModel)) {
        
            $refererModel->status = 'active';
            $refererModel->save();
            
            $user->referredBy = $refererModel->referer_id; // update referer id of user for fast access.
        
            Helper::setActivitypointsUnlimited($user->id, 'referenceregistration', '', '');//user who sign up  using referral link 
            if(Helper::getOption('minPointsReqReferralPoints') == 0) {
              Helper::setActivitypointsUnlimited($refererModel->referer_id, 'invitereference', '', '');//user who refer 
            }
        }
        $user->save();
        
        flash('Thanks for verifying your email.','success');
        return redirect('/');
    }
}
