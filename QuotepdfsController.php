<?php
/**
* @author amit gupta
* @desc Manage Quote Pdf Pages
*/
class QuotepdfsController extends RController
{
	/**
     * @var string the default layout for the views.
     */
    public $layout = '//layouts/innerMain';
    public $defaultAction = 'admin';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'rights', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model = new ConfigQuotepdf;

		if(isset($_POST['ConfigQuotepdf']))
		{	
			$model = new ConfigQuotepdf($_POST['ConfigQuotepdf']['file_type']);
			$model->attributes=$_POST['ConfigQuotepdf'];
			
			$imageUploadFile = CUploadedFile::getInstance($model, 'file_name');
			
			if($imageUploadFile !== null) {
				$model->file_name = time().'_'.$imageUploadFile;
			}

			if($model->save()) {
				$imageUploadFile->saveAs(
						Yii::app()->params['filesPath'] . 'quotes' . Yii::app()->params['sep'] . 'design' . Yii::app()->params['sep'] . $model->file_type . Yii::app()->params['sep'] . $model->file_name);
				$this->redirect(array('view','id'=>$model->id));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$oldFile = $model->file_name;
		
		if(isset($_POST['ConfigQuotepdf']))
		{
			$model->scenario = $_POST['ConfigQuotepdf']['file_type'];
			$model->attributes=$_POST['ConfigQuotepdf'];
			
			$imageUploadFile = CUploadedFile::getInstance($model, 'file_name');
			
			if($imageUploadFile !== null) {
				$oldFile = $model->file_name;
				$model->file_name = time().'_'.$imageUploadFile;
			} else {
				$oldFile = $model->file_name;
			}

			$model->scenario = 'update';
			if($model->save()) {
				if($imageUploadFile !== null) {
					$oldFielPath = Yii::app()->params['filesPath'] . 'quotes' . Yii::app()->params['sep'] . 'design' . Yii::app()->params['sep'] . $model->file_type . Yii::app()->params['sep'] . $oldFile;

					if(file_exists($oldFielPath)) {
						unlink($oldFielPath);
					}

					$imageUploadFile->saveAs(
							Yii::app()->params['filesPath'] . 'quotes' . Yii::app()->params['sep'] . 'design' . Yii::app()->params['sep'] . $model->file_type . Yii::app()->params['sep'] . $model->file_name);
				}

				$this->redirect(array('view','id'=>$model->id));
			}
		}
		
		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$model = $this->loadModel($id);
		if(empty($model)) {
			$filePath =  Yii::app()->params['filesPath'] . 'quotes' . Yii::app()->params['sep'] . 'design' . Yii::app()->params['sep'] . $model->file_type . Yii::app()->params['sep'] . $model->file_name;
			
			if(file_exists($filePath)) {
				unlink($filePath);
			}

			$model->delete();
		}

		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('ConfigQuotepdf');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model = new ConfigQuotepdf('search');
		$model->unsetAttributes();  // clear any default values

		if(isset($_GET['ConfigQuotepdf']))
			$model->attributes=$_GET['ConfigQuotepdf'];

		$this->render('admin',array(
			'model' => $model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return ConfigQuotepdf the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=ConfigQuotepdf::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param ConfigQuotepdf $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='config-quotepdf-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	/**
     * @desc sort orders of pages
     */
	public function actionSort()
	{
		if (isset($_POST['items']) && is_array($_POST['items'])) {
			$i = 1;
			foreach ($_POST['items'] as $item) {
				$configQuotepdf = ConfigQuotepdf::model()->findByPk($item);
				$configQuotepdf->position = $i;
				$configQuotepdf->save();
				$i++;
			}
		}
	}

	/**
     * @desc Common change status function
     * @return type boolean
     */
    public function actionchangeStatus($id, $status, $table)
    {
        $connection = Yii::app()->db;
        $sql = "UPDATE " . $table . " SET status ='" . $status . "' WHERE id ='" . $id . "'";
        $command = $connection->createCommand($sql);
        if ($command->execute()) {
            return true;
        } else {
        	return false;
        }
    }
}
