<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Category;
use App\Http\Controllers\Controller;
use \Serverfireteam\Panel\CrudController;
use Illuminate\Http\Request;
use App\Helpers\Helper;

class CategoryController extends CrudController
{
    /**
     * @auth mitg
     * @desc Simple code of  filter and grid part , List of all fields
     */
    public function all($entity)
    {
        parent::all($entity);

        // filter
        $this->filter = \DataFilter::source(new Category);
        $this->filter->add('name', 'Category Name', 'text');
        $this->filter->add('slug', 'Slug', 'text');
        $this->filter->add('status', 'Status', 'select')->options(array('' => 'Please select', 'active' => 'Active', 'inactive' => 'Inactive'));
        $this->filter->submit('search');
        $this->filter->reset('reset');
        $this->filter->build();

        // grid
        $this->grid = \DataGrid::source($this->filter);
        $this->grid->add('image', 'Photo');
        $this->grid->add('name', 'Category Name');
        $this->grid->add('slug', 'Slug');
        $this->grid->add('status', 'Status');
        $this->grid->orderBy('position', 'desc'); //default orderby
        $this->grid->add('created_at|strtotime|date[d-m-Y]', 'Created on',true);
        
        $this->addStylesToGrid();
        $this->grid->paginate(Helper::getOption('adminPanelPageSizeAdmin'));

        return $this->returnView();
    }
    
    /**
     * @auth mitg
     * @desc Simple code of  edit part , List of all fields
     */
    public function edit($entity)
    {
        parent::edit($entity);

        
        $this->edit = \DataEdit::source(new Category());
        $this->edit->label('Category');

        $this->edit->add('name', 'Name', 'text')->rule('required');
        $this->edit->add('slug', 'Slug', 'text')->rule('required|regex:/^[a-z-]+$/u');
        $this->edit->add('position', 'Order', 'text')->rule('required|regex:/^[0-9-]+$/u');
        $this->edit->add('status', 'Status', 'select')->options(array('active' => 'Active', 'inactive' => 'Inactive'));
        $this->edit->add('image', 'Photo', 'image')->move('uploads/category/')->preview(120, 120);

        return $this->returnEditView();
    }

}
