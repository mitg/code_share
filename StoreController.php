<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Store;
use App\Http\Controllers\Controller;
use \Serverfireteam\Panel\CrudController;
use Illuminate\Http\Request;
use App\Helpers\Helper;
use Illuminate\Http\Response;

use Illuminate\Support\Facades\Redirect;

class StoreController extends CrudController
{
    /**
     * @auth Amit Gupta
     * @desc Simple code of  filter and grid part , List of all fields
     */
    public function all($entity)
    {
        parent::all($entity);
        
        //filter
        $this->filter = \DataFilter::source(new Store);
        $this->filter->add('name', 'Store Name', 'text');
        $this->filter->add('title', 'Title', 'text');
        $this->filter->add('status', 'Status', 'select')->options(array('' => 'Please select', 'active' => 'Active', 'inactive' => 'Inactive'));
        $this->filter->submit('search');
        $this->filter->reset('reset');
        $this->filter->build();

        //grid
        $this->grid = \DataGrid::source($this->filter);
        $this->grid->add('id', 'Store Id', true);
        $this->grid->add('name', 'Store Name');
        $this->grid->add('slug', 'Slug');
        $this->grid->add('used_count', 'Used Count')->cell(function ($value, $row) {
            return $row->productCount(); });
        $this->grid->add('parent_id', 'Parent Id', true);
        $this->grid->add('position', 'Order', true);
        $this->grid->add('slug', 'Slug');
        $this->grid->orderBy('position', 'desc'); //default orderby
        $this->grid->add('isPopular', 'Featured Store')->cell(function ($value, $row) {
            return "<a class='toggle-button markStore' data-column='isPopular' data-model='store' data-id='".$row->id."'>".(($row->isPopular == '1') ? '<i class="fa fa-toggle-on"></i>':'<i class="fa fa-toggle-off"></i>')."</a>";
        });
        $this->grid->add('isTopStore', 'Top Store')->cell(function ($value, $row) {
            return "<a class='toggle-button markStore' data-column='isTopStore' data-model='store' data-id='".$row->id."'>".(($row->isTopStore == '1')?'<i class="fa fa-toggle-on"></i>':'<i class="fa fa-toggle-off"></i>')."</a>";
        });
        $this->grid->add('created_at|strtotime|date[d-m-Y]', 'Created on',true);
        $this->grid->add('status', 'Status');
        
        $this->addStylesToGrid();
        $this->grid->paginate(Helper::getOption('adminPanelPageSizeAdmin'));

        return $this->returnView();
    }

    /**
     * @auth Amit Gupta
     * @desc Simple code of  edit part , List of all fields
     */
    public function edit($entity)
    {
        parent::edit($entity);

        $this->edit = \DataEdit::source(new Store());
        $this->edit->label('Store');
        $this->edit->add('name', 'Name', 'text');
        $this->edit->add('slug', 'Slug', 'text')->rule('required|regex:/^[a-z-0-9-]+$/u');
        $this->edit->add('position', 'Order', 'text')->rule('regex:/^[0-9-]+$/u');
        $this->edit->add('meta_title', 'Meta Title', 'text');
        $this->edit->add('meta_description', 'Meta Description', 'text');
        $this->edit->add('parent_id', 'Parent Id', 'text')->rule('regex:/^[0-9-]+$/u');
        $this->edit->add('used_count', 'Used Count', 'text')->rule('regex:/^[0-9-]+$/u');
        
        $this->edit->add('title', 'Title (h1)', 'text')->rule('required');
        $this->edit->add('list_title', 'List Title (h2)', 'text');
        $this->edit->add('description', 'Store Description', 'text');
        $this->edit->add('long_description', 'Long Description', 'redactor');
        
        $this->edit->add('status', 'Status', 'select')->options(array('active' => 'Active', 'inactive' => 'Inactive'));
        $this->edit->add('image', 'Photo', 'image')->resize(160,132)->move('uploads/stores/')->preview(120, 120);
        
        return $this->returnEditView();
    }
    
    
    /**
     * @auth Amit Gupta
     * @description get all popular stores/merchant/sellers
     * @return array of objects of popular stores 
     */
    public function getPopularStores(Request $request)
    {
        $stores   = Store::active()->popular()->orderBy('position', 'asc')->get();
        return view('store.popular',['stores' => $stores]);
    }
    
    /**
     * @auth Amit Gupta
     * @description mark popular/unpopular to a stores/merchant/sellers
     * @return json status error/success 
     */
    public function markStore(Request $request)
    {
        $store = Store::where('id', $request->modelId)->first();
        
        if(empty($store)) {
            return json_encode(array('status' =>'error'));
        } else {
            switch($request->modelColumn) {
                case 'isPopular':
                    if($store->isPopular) {
                        Store::where('id', $request->modelId)->update(['isPopular' => 0]);
                    } else {
                        Store::where('id', $request->modelId)->update(['isPopular' => 1]);
                    }
                    break;
                case 'isTopStore':
                    if($store->isTopStore) {
                        Store::where('id', $request->modelId)->update(['isTopStore' => 0]);
                    } else {
                        Store::where('id', $request->modelId)->update(['isTopStore' => 1]);
                    }
                    break;
                default:
                    return json_encode(array('status' => 'error'));
                    break;
            } 
        }
        return json_encode(array('status' => 'success'));
    }
    
    /**
     * @auth Amit Gupta
     * @description get list of all stores
     * @return json status error/success 
     */
    public function getStoreList(Request $request)
    {
       $topStores   = Store::active()->popular()->top()->orderBy('position', 'asc')->orderBy('group', 'asc')->orderBy( 'name', 'asc')->get();
       $stores   = Store::active()->popular()->orderBy('position', 'asc')->orderBy('group', 'asc')->orderBy( 'name', 'asc')->get();
       
       return view('store.store-list', ['stores' => $stores, 'topStores' => $topStores]);
    }
    
    /**
     * @auth Amit Gupta
     * @description get list of all stores
     * @return json status error/success 
     */
    public function getStoreListDetails(Request $request, $listgroup)
    { 
       if($listgroup == 'top') {
            $topStores   = Store::where('isTopStore', 1)->where('isPopular', 1)->active()->popular()->orderBy('position', 'asc')->orderBy('group', 'asc')->orderBy( 'name', 'asc')->get();
            $stores   = Store::where('isTopStore', 1)->active()->orderBy('position', 'asc')->orderBy('group', 'asc')->orderBy( 'name', 'asc')->get();
       } else {
            $topStores   = Store::where('group', $listgroup)->active()->popular()->orderBy('position', 'asc')->orderBy('group', 'asc')->orderBy( 'name', 'asc')->get();
            $stores   = Store::where('group', $listgroup)->active()->orderBy('position', 'asc')->orderBy('group', 'asc')->orderBy( 'name', 'asc')->get();
       } 
       
       return view('store.detailed-store-list', ['stores' => $stores, 'topStores' => $topStores, 'listgroup' => $listgroup]);
    }
    
    
    /**
     * @auth Amit Gupta
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getStoreDetails(Request $request, $merchant)
    {
        $store   = Store::where('slug', $merchant)->active()->first();
        if(empty($store)) {
            $store   = Store::where('slug', $merchant)->first();
        }
        
        if(!empty($store) && $store->status == 'inactive' && !empty($store->parent_id)) {
            $store   = Store::where('id', $store->parent_id)->active()->first();
            return Redirect('deals/'.$store->slug, 301);
        } elseif (!empty($store) && $store->status == 'inactive') {
            $store   = Store::where('slug', $merchant)->active()->first();
        }
               
        $pageStart = \Request::get('page', 1);
        $perPage =  20;
        
        if (!$store) abort(404);
         
        $deals = empty($store->deals)? collect(new \App\Deal): $store->deals;
        $sales = empty($store->sales)? collect(new \App\Sale): $store->sales;
        $vouchers = empty($store->vouchers)?  collect(new \App\Voucher): $store->vouchers;
        
        foreach($store->children as $children) {
            $deals = $deals->merge(empty($children->deals)? collect(new \App\Deal): $children->deals);
            $sales = $sales->merge(empty($children->sales)? collect(new \App\Sale): $children->sales);
            $vouchers = $vouchers->merge(empty($children->vouchers)?  collect(new \App\Voucher): $children->vouchers);
        }
        
        $items = $deals->toBase()->merge($sales->toBase())->merge($vouchers->toBase())->sortByDesc('created_at');
        $miscs = Helper::makeCustomPagination($items, $perPage, $pageStart);
        
        return view('store.detail', ['store' => $store, 'miscs' => $miscs]);
    }
    
    /* 
     * @auth Amit Gupta
     * Get stores Results
     * @param $request
     * @return json object
     */
    public function getStore(Request $request, Response $response)
    {
        $data    = array();        
        // Gets the query string from our form submission
        $keyword = $request->input('keyword');
        
        if(empty($keyword))
            return $response->setContent($data);
        
        // Returns an array of articles that have the query string located somewhere within
        // our articles titles. Paginates them so we can break up lots of search results.
        $results = Store::where('name', "LIKE", '%'.$keyword.'%')
            ->where('status','active')
            ->orderBy('name','asc')
            ->get();

        foreach ($results as $row) {
            $data[] = array("name" => $row->name,
                "picLocation" => env('APP_URL') . env('UPLOAD_PATH') . 'stores/' . $row->image,
                "id" => $row->id
            );
        }
        
        return $response->setContent($data);
    }
}
