<?php

/**
 * This is the model class for table "config_quotepdf".
 *
 * The followings are the available columns in table 'config_quotepdf':
 * @property string $id
 * @property string $quote_type
 * @property string $page_type
 * @property string $file_type
 * @property integer $position
 * @property string $file_name
 * @property string $status
 * @property string $created
 * @property string $updated
 */
class ConfigQuotepdf extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'config_quotepdf';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('quote_type, page_type', 'required'),
			array('position', 'numerical', 'integerOnly'=>true),
			array('quote_type', 'length', 'max'=>16),
			array('page_type', 'length', 'max'=>10),
			array('file_type', 'length', 'max'=>5),
			array('file_name', 'file', 'types'=>'jpg, gif, png', 'safe' => false,'on'=>'image'),
			array('file_name', 'file', 'types'=>'pdf', 'safe' => false,'on'=>'pdf'),

			array('file_name', 'length', 'max'=>255),
			array('status', 'length', 'max'=>8),
			array('file_name', 'safe', 'on'=>'update'),
			array('created, updated', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, quote_type, page_type, file_type, position, file_name, status, created, updated', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'quote_type' => 'Quote Type',
			'page_type' => 'Page Type',
			'file_type' => 'File Type',
			'position' => 'Position',
			'file_name' => 'File Name',
			'status' => 'Status',
			'created' => 'Created',
			'updated' => 'Updated',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('quote_type',$this->quote_type,true);
		$criteria->compare('page_type',$this->page_type,true);
		$criteria->compare('file_type',$this->file_type,true);
		$criteria->compare('position',$this->position);
		$criteria->compare('file_name',$this->file_name,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('updated',$this->updated,true);
		
		// $criteria->condition = "quote_type = :quote_type";
  //   	$criteria->params=(array(':quote_type' => $quote_type));
		$criteria->order = 'position ASC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
                        'pageSize'=>50,
                ),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ConfigQuotepdf the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function behaviors()
    {
        return array(
            'AutoTimestampBehavior' => array(
                'class' => 'application.components.custom.AutoTimestampBehavior'),
            'CSaveRelationsBehavior' => array(
                'class' => 'application.components.CSaveRelationsBehavior'),
            );
    }

    /**
	 * Define scope which can be used while fetching data from db.
	 */
    public function scopes()
    {
        return array(
            'activeLoftSortASC'=>array(
                'condition'=>"quote_type='loft' AND status='active' ",
                'order'=>'position ASC',
            ),
            'activeExtensionSortASC'=>array(
                'condition'=>"quote_type='extension' AND status='active' ",
                'order'=>'position ASC',
            ),
            'activeLoftandextensionSortASC'=>array(
                'condition'=>"quote_type='loftandextension' AND status='active' ",
                'order'=>'position ASC',
            ),
            'active'=>array(
                'condition'=>"status='active' ",
            ),
            'frontquote'=>array(
                'condition'=>"page_type='frontquote' ",
            ),
            'backquote'=>array(
                'condition'=>"page_type='backquote' ",
            ),
            'positionASC'=>array(
                'order'=>"position ASC ",
            ),
        );
    }
}
